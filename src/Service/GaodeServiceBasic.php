<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-27 18:35:39
 * @LastEditTime: 2021-08-30 16:00:25
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Service/GaodeServiceBasic.php
 *
 */
namespace Zhaohangyang\ToolsPhpGaode\Service;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpGaode\GaodeBasic;

/**
 * 高德圆形围栏
 */
class GaodeServiceBasic extends GaodeBasic
{
    public $headers = [
        'content-type' => 'application/x-www-form-urlencoded',
    ];
    public function __construct($gaode_config)
    {
        $gaode_config['service_version'] = $gaode_config['service_version'] ?? '/v1';
        parent::__construct($gaode_config);
    }

    public function add($add_body)
    {
        $pai_url = $this->gaodeConfig['service_version'] . '/track/service/add';
        // $add_body =[
        //  --name --服务名称 --Service 的名字，名字在同一个 Key 下不可重复，不可为空。 --命名规则：仅支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字,不能以"_"开头，最长不得超过128个字符。 --必填 --无
        //  --desc --服务描述 --针对此 Service 的文字描述，方便用户对 Service 进行记忆。 --命名规则：仅支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字, 不能以"_"开头，最长不得超过128个字符。 --非必填  --无
        // ];
        $body = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
        ] + $add_body;

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }

    public function update($update_body)
    {
        $pai_url = $this->gaodeConfig['service_version'] . '/track/service/update';
        // $update_body =[
        //  --sid --服务的唯一编号 --不可修改，此参数用于查找 Service --即便填入此参数，用户也不可修改sid。 --必填  --无
        //  --name --服务名称 --Service 的名字，名字在同一个 Key 下不可重复，不可为空。 --命名规则：仅支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字,不能以"_"开头，最长不得超过128个字符。 --必填 --无
        //  --desc --服务描述 --针对此 Service 的文字描述，方便用户对 Service 进行记忆。 --命名规则：仅支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字, 不能以"_"开头，最长不得超过128个字符。 --非必填  --无
        // ];
        $body = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
        ] + $update_body;

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }

    public function find()
    {
        $pai_url = $this->gaodeConfig['service_version'] . '/track/service/list';
        $body    = [
            'key' => $this->gaodeConfig['key'],
        ];

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }

    public function delete($delete_body)
    {
        $pai_url = $this->gaodeConfig['service_version'] . '/track/service/delete';
        // $delete_body =[
        //  --sid --服务的唯一编号 --不可修改，此参数用于查找 Service --即便填入此参数，用户也不可修改sid。 --必填  --无
        // ];
        $body = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
        ] + $delete_body;

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }

    public function getService($name)
    {
        $service_find_res = $this->find();

        if (isset($service_find_res['data']['results']) && !empty($service_find_res['data']['results'])) {
            foreach ($service_find_res['data']['results'] as $result) {
                if ($result['name'] == $name) {
                    return $result;
                }
            }
        }
        return null;
    }

    public function getServiceId($name)
    {
        $service = $this->getService($name);
        return $service['sid'] ?? null;
    }
}
