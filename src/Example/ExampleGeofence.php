<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-31 09:47:04
 * @LastEditTime: 2021-08-31 11:52:05
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Example/ExampleGeofence.php
 *
 */

namespace Zhaohangyang\ToolsPhpGaode\Example;

use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofenceCircle;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofenceDistrict;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofencePolygon;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofencePolyline;
use Zhaohangyang\ToolsPhpGaode\Geofence\Status\GaodeGeofenceStatus;
use Zhaohangyang\ToolsPhpGaode\Service\GaodeServiceBasic;

class ExampleGeofence
{
    /**
     * 创建圆形围栏
     *
     * @param [type] $standardConfig
     * @return void
     */
    public static function addCircle($standardConfig)
    {
        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $service_basic                  = new GaodeServiceBasic($standardConfig);
        $standardConfig['geofence_sid'] = $service_basic->getServiceId('test');

        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $geofence_circle = new GaodeGeofenceCircle($standardConfig);
        $add_body        = [
            //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
            'name'   => 'test_circle',
            //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无
            'desc'   => 'test_desc',
            //     --center--围栏中心点坐标--格式X,Y--必填--无
            'center' => '116.481499,39.990475',
            //     --radius--围栏半径--单位：米，整数，取值范[1,50000]--必填--无
            'radius' => '1000',
        ];
        $create_circle_res = $geofence_circle->add($add_body);
        return $create_circle_res;
    }

    /**
     * 创建多边形围栏
     *
     * @param [type] $standardConfig
     * @return void
     */
    public static function addPolygon($standardConfig)
    {
        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $service_basic                  = new GaodeServiceBasic($standardConfig);
        $standardConfig['geofence_sid'] = $service_basic->getServiceId('test');

        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $geofence_polygon = new GaodeGeofencePolygon($standardConfig);
        $add_body         = [
            //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
            'name'   => 'test_polygon',
            //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无
            'desc'   => 'test_desc',
            //  --points --多边形顶点坐标 --格式 X1,Y1;X2,Y2;... --顶点顺序可按顺时针或逆时针排列； --1. 普通地理围栏：顶点个数在3-100个之间，外接矩形面积<100平方公里； --2. 大范围地理围栏：顶点个数在3-100个之间，外接矩形面积小于1000平方公里，请您通过工单与我们联系。
            'points' => '116.000001,39.000001;116.000002,39.000002;116.000003,39.000003;',

        ];
        $create_polygon_res = $geofence_polygon->add($add_body);
        return $create_polygon_res;
    }

    /**
     * 创建线性围栏
     *
     * @param [type] $standardConfig
     * @return void
     */
    public static function addPolyline($standardConfig)
    {
        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $service_basic                  = new GaodeServiceBasic($standardConfig);
        $standardConfig['geofence_sid'] = $service_basic->getServiceId('test');

        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $geofence_polyline = new GaodeGeofencePolyline($standardConfig);
        $add_body          = [
            //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
            'name'         => 'test_polyline',
            //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无
            'desc'         => 'test_desc',
            //     --points --线上各点坐标 --格式 X1,Y1;X2,Y2;... --线路总长度<500KM，坐标点个数取值范围 [2，100]，超出范围请您通过工单与我们联系。
            'points'       => '116.000001,39.000001;116.000002,39.000002;116.000003,39.000003;',
            //     --bufferradius  --沿线偏移距离  --单位：米  --取值范围 [1，300]
            'bufferradius' => '111',
        ];
        $create_polyline_res = $geofence_polyline->add($add_body);
        return $create_polyline_res;
    }

    /**
     * 创建行政区围栏
     *
     * @param [type] $standardConfig
     * @return void
     */
    public static function addDistrict($standardConfig)
    {
        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $service_basic                  = new GaodeServiceBasic($standardConfig);
        $standardConfig['geofence_sid'] = $service_basic->getServiceId('test');

        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $geofence_district = new GaodeGeofenceDistrict($standardConfig);
        $add_body          = [
            //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
            'name'   => 'test_district',
            //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无
            'desc'   => 'test_desc',
            //     --adcode  --行政区划编码  --参考行政区划编码表
            'adcode' => '110000',
        ];
        $create_district_res = $geofence_district->add($add_body);
        return $create_district_res;
    }

    public static function statusTerminal($standardConfig)
    {
        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];

        $service_basic                  = new GaodeServiceBasic($standardConfig);
        $standardConfig['geofence_sid'] = $service_basic->getServiceId('test');

        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];
        $geofence_status = new GaodeGeofenceStatus($standardConfig);

        $terminal_body = [
            // --tid--监控对象终端id--每次只能输入1个--必填
            'tid'      => '',
            // --gfids--围栏的唯一标识--支持一次传入多个，以","分割；--单次最多支持100个，如超出将只截取前100个作为输入。--否；--如传入，则只判断该监控对象与 gfids 所指定的围栏的关系，不分页（忽略 page 和 pagesize 参数）；--如不传，则判断该监控对象与所有绑定围栏的关系（分页返回）；
            'gfids'    => 'test_district',
            // --page--查询页数--默认1--可选，默认1
            'page'     => 1,
            // --pagesize--每页数量--单页数据数量，取值 [1, 100]--可选，默认50
            'pagesize' => 50,
        ];

        $geofence_terminal_res = $geofence_status->terminal($terminal_body);
        return $geofence_terminal_res;
    }

    public static function statusLocation($standardConfig)
    {
        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];
        $service_basic                  = new GaodeServiceBasic($standardConfig);
        $standardConfig['geofence_sid'] = $service_basic->getServiceId('test');

        // $standardConfig = [
        //      --  key-- 高德应用key
        //      --  api_url-- api基础域名
        //      --  sid-- 服务唯一编号--   sid为终端所属service唯一编号-- 必填
        // ];
        $geofence_status = new GaodeGeofenceStatus($standardConfig);

        $location_body = [
            // --location--指定坐标--格式：x,y--必填
            'location' => '116.000001,39.000001',
            // --gfids--围栏的唯一标识--支持一次传入多个，以","分割；--单次最多支持100个，如超出将只截取前100个作为输入。--否；--如传入，则只判断该监控对象与 gfids 所指定的围栏的关系，不分页（忽略 page 和 pagesize 参数）；--如不传，则判断该监控对象与所有绑定围栏的关系（分页返回）；
            'gfids'    => 'test_polyline',
            // --page--查询页数--默认1--可选，默认1
            'page'     => 1,
            // --pagesize--每页数量--单页数据数量，取值 [1, 100]--可选，默认50
            'pagesize' => 50,
        ];

        $geofence_location_res = $geofence_status->location($location_body);
        return $geofence_location_res;
    }

}
