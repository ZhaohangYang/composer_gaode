<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-27 16:27:17
 * @LastEditTime: 2021-08-31 11:09:47
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Geofence/GaodeGeofencePolygon.php
 *
 */
namespace Zhaohangyang\ToolsPhpGaode\Geofence;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofenceBasic;

/**
 * 高德多边形围栏
 */
class GaodeGeofencePolygon extends GaodeGeofenceBasic
{
    public function add($add_body)
    {
        // $add_body    = [
        //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
        //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无
        //     --points --多边形顶点坐标 --格式 X1,Y1;X2,Y2;... --顶点顺序可按顺时针或逆时针排列； --1. 普通地理围栏：顶点个数在3-100个之间，外接矩形面积<100平方公里； --2. 大范围地理围栏：顶点个数在3-100个之间，外接矩形面积小于1000平方公里，请您通过工单与我们联系。
        // ];
        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/add/polygon';
        $body    = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => $this->gaodeConfig['geofence_sid'],
        ] + $add_body;

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }
}
