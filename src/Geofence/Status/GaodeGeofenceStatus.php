<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-27 16:27:17
 * @LastEditTime: 2021-08-31 11:57:20
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Geofence/Status/GaodeGeofenceStatus.php
 *
 */
namespace Zhaohangyang\ToolsPhpGaode\Geofence\Status;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofenceBasic;

/**
 * 围栏关系判断
 */
class GaodeGeofenceStatus extends GaodeGeofenceBasic
{
    public function terminal($terminal_body)
    {
        // $terminal_body = [
        //     --key--高德Key--用户在高德地图官网申请Web服务API类型Key--必填
        //     --sid--服务唯一编号--sid为终端所属service唯一编号--必填
        //     --tid--监控对象终端id--每次只能输入1个--必填
        //     --gfids--围栏的唯一标识--支持一次传入多个，以","分割；--单次最多支持100个，如超出将只截取前100个作为输入。--否；--如传入，则只判断该监控对象与 gfids 所指定的围栏的关系，不分页（忽略 page 和 pagesize 参数）；--如不传，则判断该监控对象与所有绑定围栏的关系（分页返回）；
        //     --page--查询页数--默认1--可选，默认1
        //     --pagesize--每页数量--单页数据数量，取值 [1, 100]--可选，默认50
        // ];

        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/status/terminal';
        $body    = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => $this->gaodeConfig['geofence_sid'],
        ] + $terminal_body;

        $request = new Request('GET', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }

    public function location($location_body)
    {
        // $location_body = [
        //     --key--高德Key--用户在高德地图官网申请Web服务API类型Key--必填
        //     --sid--服务唯一编号--sid为终端所属service唯一编号--必填
        //     --location--指定坐标--格式：x,y--必填
        //     --gfids--围栏的唯一标识--支持一次传入多个，以","分割；--单次最多支持100个，如超出将只截取前100个作为输入。--否；--如传入，则只判断该监控对象与 gfids 所指定的围栏的关系，不分页（忽略 page 和 pagesize 参数）；--如不传，则判断该监控对象与所有绑定围栏的关系（分页返回）；
        //     --page--查询页数--默认1--可选，默认1
        //     --pagesize--每页数量--单页数据数量，取值 [1, 100]--可选，默认50
        // ];

        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/status/location';
        $body    = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => $this->gaodeConfig['geofence_sid'],
        ] + $location_body;

        $request = new Request('GET', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);

    }
}
