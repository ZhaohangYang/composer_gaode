<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-27 16:27:17
 * @LastEditTime: 2021-08-31 11:21:09
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Geofence/GaodeGeofencePolyline.php
 *
 */
namespace Zhaohangyang\ToolsPhpGaode\Geofence;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofenceBasic;

/**
 * 高德线性围栏
 */
class GaodeGeofencePolyline extends GaodeGeofenceBasic
{
    public function add($add_body)
    {
        // $add_body    = [
        //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
        //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无 --points
        //     --points --线上各点坐标 --格式 X1,Y1;X2,Y2;... --线路总长度<500KM，坐标点个数取值范围 [2，100]，超出范围请您通过工单与我们联系。
        //     --bufferradius  --沿线偏移距离  --单位：米  --取值范围 [1，300]
        // ];
        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/add/polyline';
        $body    = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => $this->gaodeConfig['geofence_sid'],
        ] + $add_body;

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }
}
