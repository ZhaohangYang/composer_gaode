<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-27 16:27:17
 * @LastEditTime: 2021-08-30 16:17:39
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Geofence/GaodeGeofenceCircle.php
 *
 */
namespace Zhaohangyang\ToolsPhpGaode\Geofence;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpGaode\Geofence\GaodeGeofenceBasic;

/**
 * 高德圆形围栏
 */
class GaodeGeofenceCircle extends GaodeGeofenceBasic
{
    public function add($add_body)
    {
        // $add_body    = [
        //     --name --围栏名称 --在同一个 sid 下不可重复，不可为空。 --支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符 --必填 --无
        //     --desc--围栏描述--支持中文、英文大小字母、英文下划线"_"、英文横线"-"和数字，长度不大于128个字符--否--无
        //     --center--围栏中心点坐标--格式X,Y--必填--无
        //     --radius--围栏半径--单位：米，整数，取值范[1,50000]--必填--无
        // ];
        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/add/circle';
        $body    = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => $this->gaodeConfig['key'],
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => $this->gaodeConfig['geofence_sid'],
        ] + $add_body;

        $request = new Request('POST', $pai_url, $this->headers, http_build_query($body));
        return $this->requestJsonSync($request);
    }
}
