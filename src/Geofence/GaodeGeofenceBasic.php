<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-27 18:35:39
 * @LastEditTime: 2021-08-30 16:18:47
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /tools_php_gaode/src/Geofence/GaodeGeofenceBasic.php
 *
 */
namespace Zhaohangyang\ToolsPhpGaode\Geofence;

use GuzzleHttp\Psr7\Request;
use Zhaohangyang\ToolsPhpGaode\GaodeBasic;

/**
 * 高德圆形围栏
 */
class GaodeGeofenceBasic extends GaodeBasic
{
    public $headers = [
        'content-type' => 'application/x-www-form-urlencoded',
    ];
    public function __construct($gaode_config)
    {
        $gaode_config['geofence_version'] = $gaode_config['geofence_version'] ?? '/v1';
        // sid 服务唯一编号 sid为猎鹰service唯一编号
        $this->addStandardConfig('geofence_sid');
        parent::__construct($gaode_config);
    }

    public function add($add_body)
    {
    }

    public function update($update_body)
    {
    }

    public function find($find_body)
    {
        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/list';
        // $find_body =[
        //     --outputshape --是否返回形状信息--1：是--0：否--可选，--默认0
        //     --gfids--围栏的唯一标识--支持一次传入多个，以","分割；--单次最多支持100个，如超出将只截取前100个作为输入。--否；--如传入，则只返回由 gfids 所指定的围栏，不分页（忽略 page 和 pagesize 参数）；--如不传，则分页返回所有围栏；
        //     --page--查询页数--默认1--可选，--默认1
        //     --pagesize--每页围栏数量--单页数据数量，取值 [1, 100]--可选，--默认50
        // ];
        $body = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => '',
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => '',
        ] + $find_body;

        $request = new Request('POST', $pai_url, $this->headers, $body);
        return $this->requestJsonSync($request);
    }

    public function delete($delete_body)
    {
        $pai_url = $this->gaodeConfig['geofence_version'] . '/track/geofence/delete';
        // $find_body =[
        // --gfids--围栏的唯一标识--1.支持一次传入多个，以","分割；单次最多支持100个，如超出将只截取前100个作为输入；--2.可以传入 "#all" 删除所有。
        // ];
        $body = [
            //  --高德key--用户在高德地图官网申请Web服务API类型Key--必填-- 无
            'key' => '',
            //  --sid--服务唯一编号--sid为猎鹰service唯一编号--必填--无
            'sid' => '',
        ] + $delete_body;

        $request = new Request('POST', $pai_url, $this->headers, $body);
        return $this->requestJsonSync($request);
    }

}
